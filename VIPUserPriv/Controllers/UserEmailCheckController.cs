﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VIPQBAPI.Models;


namespace VIPQBAPI.Controllers
{
    public class UserEmailCheckController : ApiController
    {
     
        public bool GetMailValidation(string email)
        {
            bool Valied = false;
            using (ebnPartnerPortal_prodEntities ebnDB = new ebnPartnerPortal_prodEntities())
            {
                if (ebnDB.UserProfiles.Where(m => m.Email == email).Count() > 0)
                {
                    Valied = ebnDB.UserProfiles.Where(o => o.Email == email).Select(o => o.SendAIMail).FirstOrDefault() ?? false;
                    //   Valied = up.SendAIMail.Value;
                }

            }

            return Valied;
        }

    }
}
