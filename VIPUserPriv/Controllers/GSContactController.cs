﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VIPQBAPI.Models;

namespace VIPQBAPI.Controllers
{
    public class GSContactController : ApiController
    {

        public string Get(string customerName, string carrierName, string orderType, DateTime? oEStartDate = null)
        {

            QBGS qBGS = new QBGS { customerName = customerName, carrierName = carrierName, orderType = orderType, oEStartDate = oEStartDate };

            if (!string.IsNullOrEmpty(customerName) && !string.IsNullOrEmpty(carrierName) && !string.IsNullOrEmpty(orderType))
                return qBGS.getGS();
            else
            {
                Logger.log.Info("parameters r null");
                return null;
            }
        }



    }
}
