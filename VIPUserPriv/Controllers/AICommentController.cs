﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VIPQBAPI.Models;

namespace VIPQBAPI.Controllers
{
    public class AICommentController : ApiController
    {


        // POST api/<controller>
        public bool Post([FromBody]QBAc model)
        {
            Logger.log.Info("Posting AI controller");

           // Logger.log.Info("Comment: " + model.Comment ?? null);
            Logger.log.Info("Posting : AICommentController ");
            Logger.log.Info("file:  "+model.FileAttachment);

            try
            {
                model.getAC();
                Logger.log.Info("REsPonsible Party "+model.aItem.ResponsibleParty);
                Logger.log.Info("Contact_Email"+ model.aItem.Contact_Email);

                if (model.aItem.ResponsibleParty.ToLower() == "partner")
                    return EmailChk.GetMailValidation(model.aItem.Contact_Email);

                else return false;

            }
            catch (System.Exception e)
            {
                Logger.log.Error("Error: for AI: " + model.QBCommentGUID);
                var st = new StackTrace(e, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                Logger.log.Info("Exception occurs in line no.: " + line);
                if (e.InnerException != null) Logger.log.Error("Inner exception: " + e.InnerException);
                Logger.log.Error("Exception occurs: " + e.Message);
                return false;
            }
        }

    }
}