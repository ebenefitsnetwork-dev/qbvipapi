﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace VIPQBAPI.Models
{
    public class QBAi
    {
        public string CarrierName { get; set; }
        public string CustomerName { get; set; }
        public DateTime? CompletionDate { get; set; }
        //   public int? Files { get; set; }
        // public DateTime? LastComment { get; set; }
        public DateTime? OeYear { get; set; }
        public string OrderType { get; set; }
        public string PartnerName { get; set; }
        public string ResName { get; set; }
        public string ContactEmail { get; set; }
        public bool MarkCompleted { get; set; }
        public int MarkCompletedF { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime DateModified { get; set; }
        public string ResponsibleParty { get; set; }
        public string Assignee { get; set; }
        public string ActionItemDescription { get; set; }
        public string Requestor { get; set; }
        public long? QBRecordID { get; set; }
        public string ProjectName { get; set; }
        public string Status { get; set; }
        public string AssignedTo { get; set; }
        public Guid? QBGUID { get; set; }
        public CustomerCarriers_View car { get; set; }
        public string OtherActionItemDescription { get; set; }



        public void getCar()
        {
            using (ebnPartnerPortal_prodEntities Ebn = new ebnPartnerPortal_prodEntities())
            {
                Logger.log.Info("get car");
                try
                {

                    DateTime oEStartDate;

                    if (OeYear.HasValue)
                        oEStartDate = OeYear.Value.Date;

                    car = Ebn.CustomerCarriers_View.Where(c => c.CarrierName == CarrierName && c.CustomerName == CustomerName
                                                                        && (OrderType.Contains("OE") ? (c.OrderType == "OE" && DbFunctions.TruncateTime(c.OEStartDate) == DbFunctions.TruncateTime(OeYear)) : c.OrderType == "EDI")
                                                                 && c.Status.ToLower() == "submitted").FirstOrDefault();
                }
                catch (System.Exception e)
                {
                    SndMail sndemail = new SndMail();
                    try
                    {
                        sndemail.Snd(Subject: "failed to get carrier GetCar");
                    }
                    catch (Exception ex)
                    {
                        Logger.log.Error("Failed to send email" + ex.Message);
                    }
                    var st = new StackTrace(e, true);
                    var frame = st.GetFrame(0);
                    var line = frame.GetFileLineNumber();
                    Logger.log.Info("Exception occurs in line no.: " + line);
                    if (e.InnerException != null) Logger.log.Error("Inner exception: " + e.InnerException);
                    Logger.log.Error("Exception occurs: " + e.Message);

                }
            }

        }

        public void geActionItem()
        {
            Logger.log.Info("start getting action item");
            string action = "new";

            MarkCompleted = MarkCompletedF == 1 ? true : false;





            getCar();
            try
            {
                using (ebnPartnerPortal_prodEntities Ebn = new ebnPartnerPortal_prodEntities())
                {
                    #region get OAI for active conn
                    try
                    {
                        //if (car != null)
                        Logger.log.Info("CarrierID: " + car.CarrierID ?? null);
                        long partnerid = Ebn.Partners.Where(p => p.PartnerName == PartnerName).Select(p => p.PartnerId).FirstOrDefault();
                        long? resid = null;

                        Logger.log.Info("ResellerNAme: " + ResName ?? null);
                        if (ResName != null)
                            resid = Ebn.Partners.Where(p => p.PartnerName == ResName && p.IsReseller == true).Select(p => p.PartnerId).FirstOrDefault();

                        // long? actionitemrecid = Ebn.ActionItems.Where(a => a.QBRecordID == QBRecordID).Select(a => a.QBRecordID).FirstOrDefault();

                        #region new
                        Logger.log.Info("QBRecordID " + QBRecordID ?? "empty");
                        if (QBRecordID == null || Ebn.ActionItems.Where(c => c.QBRecordID == QBRecordID).Count() == 0)// new 
                        {
                            Logger.log.Info("new");
                            action = "new";
                            DateTime vipstartdate = new DateTime(2017, 10, 15);
                            StartDate = DateTime.Now;
                            DateModified = DateTime.Now;

                            if (!Status.ToLower().Contains("close"))// && ac.ResponsibleParty.ToLower() != "eBN")
                            {
                                #region AI
                                try
                                {
                                    Console.WriteLine("NEw action item for actionQBid: " + QBGUID + " EDI");
                                    Logger.log.Info("NEw action item for actionQBid: " + QBGUID + " EDI");

                                    ActionItem Actitem = new ActionItem();
                                    Actitem.CarrierId = car.CarrierID;
                                    Actitem.QBRecordID = QBRecordID;
                                    Actitem.ProjectName = ProjectName;
                                    Actitem.ResponsibleParty = ResponsibleParty;

                                    //if (!string.IsNullOrEmpty(ActionItemDescription)) Actitem.ActionItemDescription = ActionItemDescription;
                                    //else Actitem.ActionItemDescription = "NA";


                                   if (!string.IsNullOrEmpty(ActionItemDescription) && ActionItemDescription == "Other" && !string.IsNullOrEmpty(OtherActionItemDescription)) Actitem.ActionItemDescription = OtherActionItemDescription;
                                    else if(!string.IsNullOrEmpty(ActionItemDescription)) Actitem.ActionItemDescription = ActionItemDescription;
                                    else Actitem.ActionItemDescription = "NA";

                                    Actitem.Status = Status;
                                    Actitem.StartDate = DateTime.Now;
                                    Actitem.DueDate = DueDate;
                                    Actitem.QBRecordID = QBRecordID;
                                    Actitem.Requestor = Requestor;
                                    Actitem.CarrierId = car.CarrierID;
                                    Actitem.Carrier_Name = CarrierName;
                                    Actitem.Customer_Name = CustomerName;
                                    Actitem.OrderId = car.OrderID;
                                    Actitem.partnerId = partnerid;
                                    Actitem.resellerID = resid;
                                    Actitem.Assignee = Assignee;
                                    Actitem.DateModified = DateModified;
                                    // Actitem.lastComment = LastComment;
                                    //  Actitem.files = Files;
                                    Actitem.Contact_Email = ContactEmail;
                                    Actitem.QBGUID = QBGUID;
                                    Ebn.ActionItems.Add(Actitem);
                                    Ebn.SaveChanges();

                                    Logger.log.Info("Insert Action item " + QBGUID + " , for carrier " + car.CarrierName + " , Project name: " + ProjectName + "' to table [ActionItems] ");
                                }
                                catch (DbEntityValidationException e)
                                {
                                    foreach (var eve in e.EntityValidationErrors)
                                    {
                                        foreach (var ve in eve.ValidationErrors)
                                        {
                                            var st = new StackTrace(e, true);
                                            var frame = st.GetFrame(0);
                                            var line = frame.GetFileLineNumber();
                                            if (e.InnerException != null) Logger.log.Error("Inner exception: " + e.InnerException);
                                            Logger.log.Error("Exception occurs in line: " + line);
                                            Logger.log.Error("Error:can not insert Action   for ActionID: " + QBGUID + " s- Property: " + ve.PropertyName + ", Error:" + ve.ErrorMessage);

                                        }
                                    }

                                    SndMail sndemail = new SndMail();
                                    try
                                    {
                                        sndemail.Snd();
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.log.Error("Failed to send email" + ex.Message);
                                    }
                                }
                                catch (System.Exception e)
                                {
                                    var st = new StackTrace(e, true);
                                    var frame = st.GetFrame(0);
                                    var line = frame.GetFileLineNumber();
                                    Logger.log.Info("Exception occurs in line no.: " + line);
                                    if (e.InnerException != null) Logger.log.Error("Inner exception: " + e.InnerException);
                                    Logger.log.Error("Exception occurs: " + e.Message);

                                }
                                #endregion
                            }

                            else if (Status.ToLower().Contains("close") && StartDate > vipstartdate)
                            {
                                Logger.log.Info("new AI" + vipstartdate);
                                try
                                {
                                    Console.WriteLine("NEw action item for actionQBid: " + QBRecordID + " EDI");
                                    Logger.log.Info("NEw action item for actionQBid: " + QBRecordID + " EDI");

                                    ActionItem Actitem = new ActionItem();
                                    Actitem.CarrierId = car.CarrierID;
                                    Actitem.QBRecordID = QBRecordID;
                                    Actitem.ProjectName = ProjectName;
                                    Actitem.ResponsibleParty = ResponsibleParty;

                                    if (!string.IsNullOrEmpty(ActionItemDescription)) Actitem.ActionItemDescription = ActionItemDescription;
                                    else Actitem.ActionItemDescription = "NA";

                                    Actitem.Status = Status;
                                    Actitem.StartDate = StartDate;
                                    Actitem.DueDate = DueDate;
                                    Actitem.Requestor = Requestor;
                                    Actitem.CarrierId = car.CarrierID;
                                    Actitem.Carrier_Name = car.CarrierName;
                                    Actitem.Customer_Name = car.CustomerName;
                                    Actitem.OrderId = car.OrderID;
                                    Actitem.partnerId = partnerid;
                                    Actitem.resellerID = resid;
                                    Actitem.Assignee = Assignee;
                                    Actitem.DateModified = DateModified;
                                    // Actitem.lastComment = LastComment;
                                    // Actitem.files = Files;
                                    Actitem.QBGUID = QBGUID;
                                    Actitem.QBRecordID = QBRecordID;
                                    Actitem.Contact_Email = ContactEmail;

                                    Ebn.ActionItems.Add(Actitem);
                                    Ebn.SaveChanges();
                                    Logger.log.Info("Insert Action item " + QBRecordID + " , for carrier " + car.CarrierName + " , Project name: " + ProjectName + "' to table [ActionItems] ");



                                }
                                catch (DbEntityValidationException e)
                                {
                                    foreach (var eve in e.EntityValidationErrors)
                                    {
                                        foreach (var ve in eve.ValidationErrors)
                                        {
                                            var st = new StackTrace(e, true);
                                            var frame = st.GetFrame(0);
                                            var line = frame.GetFileLineNumber();
                                            if (e.InnerException != null) Logger.log.Error("Inner exception: " + e.InnerException);
                                            Logger.log.Error("Exception occurs in line: " + line);
                                            Logger.log.Error("Error:can not insert Action   for ActionID: " + QBRecordID + " s- Property: " + ve.PropertyName + ", Error:" + ve.ErrorMessage);

                                        }
                                    }

                                    SndMail sndemail = new SndMail();
                                    try
                                    {
                                        sndemail.Snd();
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.log.Error("Failed to send email" + ex.Message);
                                    }
                                }
                                catch (System.Exception e)
                                {
                                    SndMail sndemail = new SndMail();
                                    try
                                    {
                                        sndemail.Snd();
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.log.Error("Failed to send email" + ex.Message);
                                    }
                                    var st = new StackTrace(e, true);
                                    var frame = st.GetFrame(0);
                                    var line = frame.GetFileLineNumber();
                                    Logger.log.Info("Exception occurs in line no.: " + line);
                                    if (e.InnerException != null) Logger.log.Error("Inner exception: " + e.InnerException);
                                    Logger.log.Error("Exception occurs: " + e.Message);

                                }
                            }

                        }
                        #endregion
                        #region update
                        else
                        {  //update
                            Logger.log.Info("update");
                            Logger.log.Info("Due Date :" + DueDate ?? null);
                            try
                            {
                                action = "update";
                                ActionItem actionitemrec = null;

                                if (QBRecordID != null)
                                    actionitemrec = Ebn.ActionItems.Where(a => a.QBRecordID == QBRecordID).FirstOrDefault();
                                if (actionitemrec == null && QBGUID != null)
                                    actionitemrec = Ebn.ActionItems.Where(a => a.QBGUID == QBGUID).FirstOrDefault();

                                if (actionitemrec != null)
                                {
                                    Logger.log.Info("update action item for actionQBid: " + QBRecordID);
                                    Logger.log.Info("markcompleted: " + MarkCompleted);
                                    if ((!MarkCompleted && Status.ToLower() == "open") || Status.ToLower() == "closed")
                                    {
                                        actionitemrec.CarrierId = car.CarrierID;
                                        actionitemrec.ProjectName = ProjectName;

                                      //  actionitemrec.ActionItemDescription = ActionItemDescription != null ? ActionItemDescription : "N/A";

                                       if (!string.IsNullOrEmpty(ActionItemDescription) && ActionItemDescription == "Other" && !string.IsNullOrEmpty(OtherActionItemDescription)) actionitemrec.ActionItemDescription = OtherActionItemDescription;
                                        else if(!string.IsNullOrEmpty(ActionItemDescription)) actionitemrec.ActionItemDescription = ActionItemDescription;
                                        else actionitemrec.ActionItemDescription = "NA";

                                        actionitemrec.StartDate = StartDate;
                                        actionitemrec.DueDate = DueDate;
                                        actionitemrec.Requestor = Requestor;
                                        actionitemrec.CarrierId = car.CarrierID;
                                        actionitemrec.OrderId = car.OrderID;
                                        actionitemrec.partnerId = partnerid;
                                        actionitemrec.Carrier_Name = car.CarrierName;
                                        actionitemrec.Customer_Name = car.CustomerName;
                                        actionitemrec.resellerID = resid;
                                        actionitemrec.DateModified = DateModified;
                                        //     actionitemrec.files = Files;
                                        actionitemrec.Status = Status;
                                        actionitemrec.QBGUID = QBGUID;
                                        actionitemrec.QBRecordID = QBRecordID;

                                    }
                                    actionitemrec.Assignee = Assignee;
                                    actionitemrec.ResponsibleParty = ResponsibleParty;



                                    Ebn.SaveChanges();
                                    Logger.log.Info("Update Action item " + QBRecordID + " , for carrier " + car.CarrierName + " , Project name: " + ProjectName + "' to table [ActionItems] ");
                                }
                                else
                                {
                                    Logger.log.Info("No updates on Action item " + QBRecordID + " , for carrier " + car.CarrierName + " , Project name: " + ProjectName);

                                }


                            }
                            catch (DbEntityValidationException e)
                            {
                                foreach (var eve in e.EntityValidationErrors)
                                {
                                    foreach (var ve in eve.ValidationErrors)
                                    {
                                        var st = new StackTrace(e, true);
                                        var frame = st.GetFrame(0);
                                        var line = frame.GetFileLineNumber();
                                        Logger.log.Error("Exception occurs in line: " + line);
                                        if (e.InnerException != null) Logger.log.Error("Inner exception: " + e.InnerException);
                                        Logger.log.Error("Error:can not " + action + " Action Comment  for ActionID: " + QBRecordID + " s- Property: " + ve.PropertyName + ", Error:" + ve.ErrorMessage);

                                    }
                                }
                                SndMail sndemail = new SndMail();
                                try
                                {
                                    sndemail.Snd();
                                }
                                catch (Exception ex)
                                {
                                    Logger.log.Error("Failed to send email" + ex.Message);
                                }
                            }
                            catch (System.Exception e)
                            {
                                SndMail sndemail = new SndMail();
                                try
                                {
                                    sndemail.Snd();
                                }
                                catch (Exception ex)
                                {
                                    Logger.log.Error("Failed to send email" + ex.Message);
                                }
                                var st = new StackTrace(e, true);
                                var frame = st.GetFrame(0);
                                var line = frame.GetFileLineNumber();
                                Logger.log.Info("Exception occurs in line no.: " + line);
                                if (e.InnerException != null) Logger.log.Error("Inner exception: " + e.InnerException);
                                Logger.log.Error("Exception occurs: " + e.Message);

                            }

                        }
                        #endregion



                    }
                    //here
                    catch (System.Exception e)
                    {
                        SndMail sndemail = new SndMail();
                        try
                        {
                            sndemail.Snd();
                        }
                        catch (Exception ex)
                        {
                            Logger.log.Error("Failed to send email" + ex.Message);
                        }
                        var st = new StackTrace(e, true);
                        // Get the top stack frame
                        var frame = st.GetFrame(0);
                        // Get the line number from the stack frame t
                        var line = frame.GetFileLineNumber();
                        Logger.log.Info(line);
                        Logger.log.Error("Exception occurs: " + e.Message);
                        if (e.InnerException != null) Logger.log.Error("Inner exception: " + e.InnerException);


                    }




                    #endregion
                    #region delete actionitems for cancelled conn
                    //try
                    //{
                    //    logger.log.Info("start deleting AI for cancelled con");
                    //    List<long> cancelledconnAI = Ebn.ActionItems.Where(c => c.Carrier.StatusId == 3).Select(c => (long)c.CarrierId).ToList();
                    //    if (cancelledconnAI.Count() != 0)
                    //    {
                    //        foreach (long cancelledid in cancelledconnAI)
                    //        {
                    //            List<ActionItem> conai = Ebn.ActionItems.Where(a => a.CarrierId == cancelledid).ToList();
                    //            // List<ActionItem> conai = origAI;

                    //            foreach (var ac in conai)
                    //            {
                    //                Ebn.ActionItems.Remove(ac);
                    //                Ebn.SaveChanges();
                    //            }
                    //        }
                    //    }
                    //    else
                    //        logger.log.Info("No AI for cancelled con");


                    //}
                    //catch (Exception ex)
                    //{
                    //    logger.log.Error("Exception occurs: " + ex.Message);
                    //    logger.log.Error("Error deleteing  AI for cancelled conn");
                    //}
                    #endregion

                }
            }
            catch (Exception e)
            {
                var st = new StackTrace(e, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame t
                var line = frame.GetFileLineNumber();
                Logger.log.Info(line);
                Logger.log.Error("Exception occurs: " + e.Message);
                if (e.InnerException != null) Logger.log.Error("Inner exception: " + e.InnerException);
                Logger.log.Error("Adding Action comments in QB failed");



                SndMail sndemail = new SndMail();
                try
                {
                    sndemail.Snd(Subject: "geActionItem failed");
                }
                catch (Exception ex)
                {
                    Logger.log.Error("Failed to send email" + ex.Message);
                    Logger.log.Error("Stack trace=: " + ex.StackTrace);
                    if (e.InnerException != null) Logger.log.Error("Inner exception: " + e.InnerException);

                }
            }

        }


    }
}
