﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mail;
using System.Web;

namespace VIPQBAPI.Models
{
    public class SndMail :IDisposable
    {
        /*
        public string MsgTo { get; set; }
        public string Msgbody { get; set; }
        * 
        */
        private static SmtpSection smtpSection; // ebnnotification
        private static MailMessage Msg;
        private static SmtpClient SmtpClnt;
        System.Net.Mail.Attachment attachment;


        public bool Snd(string EmailTo = null, string Subject = null, string projectName = null, string contactName = null, string startDate = null, string actionItemDescription = null, string link = null, string requester = null, string filePath = null, string fileName = null, string fileDescription = null, string comment = null, bool newAI = false)
        {
            string DirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string logPath = ConfigurationManager.AppSettings["logPath"];
            string eBNBcc = ConfigurationManager.AppSettings["eBNMail"];
            string eBNBto = ConfigurationManager.AppSettings["eBNMail"];

            try
            {
                using (SmtpClnt = new SmtpClient())
                {
                    using (Msg = new MailMessage())
                    {
                        Msg.IsBodyHtml = true;
                      
                            // Msg.Bcc.Add(new MailAddress(eBNBcc));                     
                            //Msg.To.Add(new MailAddress("e.adel84@gmail.com"));
                            Msg.To.Add(new MailAddress(eBNBto));

                            if(string.IsNullOrEmpty(Subject))
                            Msg.Subject = "Error in VIP API , Please check log file";
                            else
                            Msg.Subject = Subject;



                        Msg.Body = "API failed";
                            //  attachment = new System.Net.Mail.Attachment(@"\QBData-%date{yyyyMMdd_HHmmss}.log");
                           // attachment = new System.Net.Mail.Attachment(logPath);
                          //  Msg.Attachments.Add(attachment);

                        SmtpClnt.Send(Msg);


                    }
                }
             
                return true;
            }
            catch (Exception e)
            {

                if (e.InnerException != null) Logger.log.Error("Inner EXc=: " + e.InnerException);

                var st = new StackTrace(e, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame t
                var line = frame.GetFileLineNumber();

                Logger.log.Error("Err mes=: " + e.Message);
                Logger.log.Error("Stack trace=: " + e.StackTrace);
                return false;
            }


        }

        public void Dispose()
        {
            // variables will dispose automatic
            // don't add static objects at this function - because no need for dispose it and  create it again .


            // system will remove objects from garbage collector when system don't use it for a long time  
            GC.SuppressFinalize(this);

        }

    }
}